using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour
{
    private const float SPEED = 1;

    [SerializeField] private SpriteRenderer m_sprite = null;
    [SerializeField] private Transform m_tDvd = null;
    private float m_speed = SPEED;
    [SerializeField] private Sprite[] m_sprites = new Sprite[5];

    int m_index = 0;

    Vector3 m_dir = Vector3.zero;
    Vector2 m_dvdSizeHalf = Vector2.zero;

    Camera m_camera = null;



    // Start is called before the first frame update
    void Start()
    {
        m_camera = Camera.main;

        m_dir = new Vector3(1, 1, 0);
        m_dir.Normalize();

        m_dvdSizeHalf = m_tDvd.GetComponent<SpriteRenderer>().sprite.bounds.size;
        m_dvdSizeHalf *= 0.5f;

        //Debug.Log(m_dvdSize + " " + m_camera.orthographicSize);
    }

    // Update is called once per frame
    void Update()
    {
        float halfWidth = m_camera.aspect * m_camera.orthographicSize;

        Vector3 pos = m_tDvd.position;

        if (m_tDvd.position.y + m_dvdSizeHalf.y >= m_camera.orthographicSize )
        {
            pos.y = m_camera.orthographicSize - m_dvdSizeHalf.y;
            m_dir.y *= -1;
            ChangeSprite();
        }
        else if (m_tDvd.position.y - m_dvdSizeHalf.y <= -m_camera.orthographicSize)
        {
            pos.y = -m_camera.orthographicSize + m_dvdSizeHalf.y;
            m_dir.y *= -1;
            ChangeSprite();
        }

        if (m_tDvd.position.x + m_dvdSizeHalf.x >= halfWidth)
        {
            pos.x = halfWidth - m_dvdSizeHalf.x;
            m_dir.x *= -1;
            ChangeSprite();
        }
        else if (m_tDvd.position.x - m_dvdSizeHalf.x <= -halfWidth)
        {
            pos.x = -halfWidth + m_dvdSizeHalf.x;
            m_dir.x *= -1;
            ChangeSprite();
        }

        //m_camera.orthographicSize;

        m_tDvd.position = pos + m_dir * m_speed * Time.deltaTime;

        if( Input.GetKey(KeyCode.UpArrow ) )
        {
            m_speed += 0.05f;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            m_speed -= 0.05f;
        }
        if (Input.GetKey(KeyCode.R))
        {
            m_speed = SPEED;
        }
    }

    private void ChangeSprite()
    {
        m_index += 1;
        if (m_index >= m_sprites.Length)
        {
            m_index = 0;
        }
        m_sprite.sprite = m_sprites[m_index];
    }
}
