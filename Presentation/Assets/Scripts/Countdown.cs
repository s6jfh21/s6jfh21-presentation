using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Countdown : MonoBehaviour
{
    [SerializeField] private Text m_text;
    int m_counter = 60;

    // Start is called before the first frame update
    void Awake()
    {
        m_text.gameObject.SetActive(false);
    }

    void StartCountdown()
    {
        m_counter = 60;
        m_text.text = m_counter.ToString();
        m_text.gameObject.SetActive(true);
        CancelInvoke();
        InvokeRepeating("Tick", 1, 1);

        AudioControl.Instance().PlayTick();
    }

    // Update is called once per frame
    void Tick()
    {
        m_counter--;

        if( m_counter < 0 )
        {
            CancelInvoke();
            if (m_text != null)
            {
                m_text.gameObject.SetActive(false);
            }

            AudioControl.Instance().PlayHorn();
        }
        if (m_text != null)
        {
            m_text.text = m_counter.ToString();
            m_text.transform.DOPunchScale(Vector3.one * 0.1f, 0.35f, 6, 1);
        }
        AudioControl.Instance().PlayTick();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.C))
        {
            StartCountdown();
        }
    }
}
