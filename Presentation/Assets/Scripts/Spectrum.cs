using UnityEngine;
using System.Collections;

public class Spectrum : MonoBehaviour
{
    private const int NUM_SAMPLES = 64; //Min: 64, Max: 8192

    private AudioSource m_aSource;

    private float[] m_freqData;
    private float[] m_band;

    //private GameObject[] g;

    [SerializeField] private Transform m_tDvd = null;

    [SerializeField]
    private bool m_bDebug;

    void Start()
    {
        m_aSource = GetComponent<AudioSource>();

        m_freqData = new float[NUM_SAMPLES];
        int freqDataLength = m_freqData.Length;

        // Checks n is a power of 2 in 2's complement format.
        if ((freqDataLength & (freqDataLength - 1)) != 0)
        {
            Debug.LogError("m_freqData length " + freqDataLength + " is not a power of 2!!! Min: 64, Max: 8192.");
            return;
        }

        int k = 0;
        for (int j = 0; j < m_freqData.Length; j++)
        {
            freqDataLength = freqDataLength / 2;
            if (freqDataLength <= 0) { break; }
            k++;

            //print( "LENGTH: " + freqDataLength + " j: " + j + " k: " + k );
        }

        m_band = new float[k + 1];
        //g = new GameObject[k + 1];

        for (int i = 0; i < m_band.Length; i++)
        {
            m_band[i] = 0;
            /*
			g[i] = GameObject.CreatePrimitive( PrimitiveType.Sphere );
			g[i].renderer.material.SetColor( "_Color", Color.cyan );
			g[i].transform.position = new Vector3( i, 0, 0 );*/
        }

        InvokeRepeating("check", 0.0f, 1.0f / 60.0f); // update at 15 fps
    }

    private void OnGUI()
    {
        if (!m_bDebug) { return; }

        float time = GUI.HorizontalSlider(new Rect(20, 20, 200, 100), m_aSource.time, 0, m_aSource.clip.length);

        if (GUI.changed)
        {
            GetComponent<AudioSource>().time = time;
        }
    }

    private void check()
    {
        m_aSource.GetSpectrumData(m_freqData, 0, FFTWindow.Rectangular);

        int k = 0;
        int crossover = 2;

        for (int i = 0; i < m_freqData.Length; i++)
        {
            float d = m_freqData[i];
            float b = m_band[k];

            // find the max as the peak value in that frequency band.
            m_band[k] = (d > b) ? d : b;

            if (i > (crossover - 3))
            {
                k++;
                crossover *= 2;   // frequency crossover point for each band.
                                  //Vector3 tmp = new Vector3( g[k].transform.position.x, m_band[k] * 32, g[k].transform.position.z );
                                  //g[k].transform.position = tmp;

                m_band[k] = 0;
            }
        }

        if (m_tDvd != null)
        {
            m_tDvd.transform.localScale = Vector3.one + Vector3.one * m_band[1] * 0.5f;
        }
    }
    
	void Update() {
        /*
		float[] spectrum = new float[1024];
		m_aSource.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
		int i = 1;
		while (i < 1023) {
			Debug.DrawLine(new Vector3(i - 1, spectrum[i] + 10, 0),
			               new Vector3(i, spectrum[i + 1] + 10, 0), Color.red);

			Debug.DrawLine(new Vector3(i - 1, Mathf.Log(spectrum[i - 1]) + 10, 2),
			               new Vector3(i, Mathf.Log(spectrum[i]) + 10, 2), Color.cyan);

			Debug.DrawLine(new Vector3(Mathf.Log(i - 1), spectrum[i - 1] - 10, 1),
			               new Vector3(Mathf.Log(i), spectrum[i] - 10, 1), Color.green);

			Debug.DrawLine(new Vector3(Mathf.Log(i - 1), Mathf.Log(spectrum[i - 1]), 3),
			               new Vector3(Mathf.Log(i), Mathf.Log(spectrum[i]), 3), Color.yellow);
			i++;
		}*/

        if (Input.GetKey(KeyCode.UpArrow))
        {
            m_aSource.pitch += 0.001f;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            m_aSource.pitch -= 0.001f;
        }
        if (Input.GetKey(KeyCode.R))
        {
            m_aSource.pitch = 1;
        }
    }

}