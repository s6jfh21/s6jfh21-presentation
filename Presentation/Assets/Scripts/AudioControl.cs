using UnityEngine;
using System.Collections;

public class AudioControl : MonoBehaviour
{
    [SerializeField] private AudioSource m_aSourceBgm;
    [SerializeField] private AudioSource m_aSourceTick;
    [SerializeField] private AudioSource m_aSourceHorn;

    private static AudioControl s_instance;

    void Awake()
    {
        if (s_instance == null)
        {
            s_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (s_instance != this)
        {
            Destroy(gameObject);
        }
    }
 
    
	void Update() {

        if (Input.GetKey(KeyCode.UpArrow))
        {
            m_aSourceBgm.pitch += 0.001f;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            m_aSourceBgm.pitch -= 0.001f;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            //m_aSourceBgm.volume += 0.1f;
            //m_aSourceTick.volume += 0.1f;
            AudioListener.volume += 0.1f;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            //m_aSourceBgm.volume -= 0.1f;
            //m_aSourceTick.volume -= 0.1f;
            AudioListener.volume -= 0.1f;
        }
        if (Input.GetKey(KeyCode.R))
        {
            m_aSourceBgm.pitch = 1;
        }
        if (Input.GetKey(KeyCode.H))
        {
            PlayHorn();
        }
    }

    public void PlayTick()
    {
        m_aSourceTick.Play();
    }

    public void PlayHorn()
    {
        m_aSourceHorn.Play();
    }

    public static AudioControl Instance()
    {
        return s_instance;
    }
}